# Maze Walker

Play online: https://icxon.itch.io/maze-walker

Uses `Cocos2D-JS 3.13` engine and `javascript-astar` npm package

* https://www.cocos2d-x.org/download
* https://www.npmjs.com/package/javascript-astar

_________________________________________

There is no bundler and no dependency manager in this project.
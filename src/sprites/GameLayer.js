const GameLayer = cc.Layer.extend({
    ctor: function (controller) {
        this._super();

        this._player = new Player(0, 0, controller);
        this.addChild(this._player, 1);

        this.scheduleUpdate();
    },

    update(dt) {
        let size = cc.director.getWinSize();

        if (this._player.x > size.width / 2) {
            this.x = size.width / 2 - this._player.x;
        } else {
            this.x = 0;
        }
        if (this._player.y > size.height / 2) {
            this.y = size.height / 2 - this._player.y;
        } else {
            this.y = 0;
        }
    },

    setMaze(maze) {
        this.resetMaze();

        if (maze === null) return;

        this._mazeLayer = new MazeLayer(maze);
        this.addChild(this._mazeLayer);
    },

    animateSolution(path, start, finish) {
        this._mazeLayer.tileTintUp(start.gx, start.gy);

        for (let step of path) {
            this._mazeLayer.tileTintUp(step.x, step.y);
        }

        this._player.setGridPosition(start.gx, start.gy);
        this._player.followPath(path);
    },

    onTileVisit(gx, gy) {
        this._mazeLayer.tileTintDown(gx, gy);
    },

    resetMaze() {
        this._player.cleanup();
        if (this._mazeLayer) this.removeChild(this._mazeLayer);
        this._mazeLayer = null;
    }

});

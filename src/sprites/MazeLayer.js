const MazeLayer = cc.Layer.extend({

    ctor: function (maze) {
        this._super();

        this._tiles = ArrayUtils.make2dArray(maze.cols, maze.rows);

        for (let gy = 0; gy < maze.rows; gy++) {
            for (let gx = 0; gx < maze.cols; gx++) {

                let tile = this._produceTile(maze.cellAt(gx, gy));
                tile.setPosition(maze.worldPosition(gx, gy));
                this.addChild(tile);

                this._tiles[gx][gy] = tile;
            }
        }
    },

    tileTintUp(gx, gy) {
        this._tiles[gx][gy].runAction(new cc.TintTo(1, 255, 200, 200));
    },

    tileTintDown(gx, gy) {
        this._tiles[gx][gy].runAction(new cc.TintTo(1, 50, 255, 255));
    },

    _produceTile(type) {
        let frame = Game.res.grass_png;

        if (type === CELL_EMPTY) {
            frame = Game.res.sand_png;
        }
        let tile = cc.Sprite.create(frame);
        tile.setAnchorPoint(0.5);
        return tile;
    }

});

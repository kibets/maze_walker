const GameUi = cc.Layer.extend({
    ctor: function () {
        this._super();

        let size = cc.director.getWinSize();

        this._message = cc.LabelTTF.create("", "Arial", 40);
        this._message.setPosition(size.width / 2, size.height / 2);
        this.addChild(this._message);
    },

    showMessage(msg) {
        if (msg === null) {
            this._message.setString("");
        } else {
            this._message.setString(msg);
        }
    }
});

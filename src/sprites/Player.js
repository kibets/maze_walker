const P_IDLE = "idle";
const P_WALK_DOWN = "walk_down";
const P_WALK_UP = "walk_up";
const P_WALK_LEFT = "walk_left";
const P_WALK_RIGHT = "walk_right";

const P_START_DELAY = 1.5; // seconds
const P_MOVE_TIME = 0.5; // seconds

const Player = cc.Sprite.extend({

    ctor: function (gx, gy, controller) {
        this._super(Game.res.hero_png, cc.rect(0, 0, TILE_SIZE, TILE_SIZE));
        this._controller = controller;
        this._animations = {};

        this.setAnchorPoint(0.5);

        this.setGridPosition(gx, gy);

        this._initAnimation(P_IDLE, 0, 1);
        this._initAnimation(P_WALK_DOWN, 0, 4);
        this._initAnimation(P_WALK_LEFT, 1, 4);
        this._initAnimation(P_WALK_UP, 2, 4);
        this._initAnimation(P_WALK_RIGHT, 3, 4);

        this._currAnimKey = null;
        this._currAnim = null;
    },

    setGridPosition(gx, gy) {
        this.setPosition(this._controller.worldPosition(gx, gy));
    },

    followPath(path) {
        let acts = [];

        acts.push(new cc.CallFunc(() => this._switchAnimation(null)));
        acts.push(new cc.DelayTime(P_START_DELAY));

        for (let step of path) {
            let nextPoint = this._controller.worldPosition(step.x, step.y);

            acts.push(new cc.CallFunc(() => this._switchAnimation(nextPoint)));
            acts.push(new cc.MoveTo(P_MOVE_TIME, nextPoint));
            acts.push(new cc.CallFunc(() => this._centerInCell()));
            acts.push(new cc.CallFunc(() => this._onTileVisit(step.x, step.y)));
        }
        acts.push(new cc.CallFunc(() => this._switchAnimation(null)));

        this.runAction(cc.Sequence.create(acts));
    },

    _onTileVisit(gx, gy) {
        this._controller.onTileVisit(gx, gy);
    },

    // fix for MoveTo floating point precision
    _centerInCell() {
        this.x = Math.round(this.x);
        this.y = Math.round(this.y);
    },

    _switchAnimation(nextPoint) {
        let anim = P_IDLE;

        if (nextPoint === null) nextPoint = cc.p(this.x, this.y);

        if (nextPoint.x > this.x) anim = P_WALK_RIGHT;
        if (nextPoint.x < this.x) anim = P_WALK_LEFT;
        if (nextPoint.y > this.y) anim = P_WALK_UP;
        if (nextPoint.y < this.y) anim = P_WALK_DOWN;

        if (this._currAnimKey !== anim) {
            this.stopAction(this._currAnim);
            this._currAnimKey = anim;
            this._currAnim = this._animations[anim].repeatForever();
            this.runAction(this._currAnim);
        }
    },

    _initAnimation(key, sheetColumn, framesCount) {
        let frameRects = [];

        for (let row = 0; row < framesCount; row++) {
            frameRects.push(cc.rect(sheetColumn * TILE_SIZE, TILE_SIZE * row, TILE_SIZE, TILE_SIZE));
        }

        let texture = this.getTexture();

        let animFrames = [];
        for (let frameRect of frameRects) {
            let spriteFrame = new cc.SpriteFrame(texture, frameRect);
            let animFrame = new cc.AnimationFrame();
            animFrame.initWithSpriteFrame(spriteFrame, 1, null);
            animFrames.push(animFrame);
        }
        let spAnim = new cc.Animation(animFrames, 0.12);
        this._animations[key] = new cc.Animate(spAnim);
    }
});

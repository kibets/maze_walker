class Maze {
    constructor(grid) {
        this._grid = grid;

        this._graph = new Graph(grid);

        this._cols = grid.length;
        this._rows = grid[0].length;
    }

    get cols() {
        return this._cols;
    }

    get rows() {
        return this._rows;
    }

    get startPoint() {
        if (this.rows > 1) {
            return { gx: 0, gy: 1 };
        } else {
            return { gx: 0, gy: 0 };
        }
    }

    get finishPoint() {
        if (this.rows === 1) {
            return { gx: this.cols - 1, gy: this.rows - 1 };
        } else {
            return { gx: this.cols - 1, gy: this.rows - 2 };
        }
    }

    isOutside(gx, gy) {
        return gx < 0 || gy < 0 || gx >= this.cols || gy >= this.rows;
    }

    cellAt(gx, gy) {
        if (this.isOutside(gx, gy)) return null;

        return this._grid[gx][gy];
    }

    worldPosition(gx, gy) {
        let x = gx * TILE_SIZE;
        let y = this._rows * TILE_SIZE - gy * TILE_SIZE;

        return cc.p(x, y);
    }

    findPath(gx1, gy1, gx2, gy2) {
        let start = this._graph.grid[gx1][gy1];
        let end = this._graph.grid[gx2][gy2];
        return astar.search(this._graph, start, end, { heuristic: astar.heuristics.manhattan });
    }
}

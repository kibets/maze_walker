// GENERIC BOILERPLATE

// vars --------------------------------------------- //

Game = {};
Game.scenes = [];
Game.scenes[1] = {};
Game.layers = [];
Game.layers[1] = {};

// res --------------------------------------------- //

Game.res = {
    grass_png: "assets/grass.png",
    sand_png: "assets/sand.png",
    hero_png: "assets/hero.png",

    maze_txt: "assets/maze.txt",
};

Game.g_resources = [];

for (var i in Game.res) {
    Game.g_resources.push(Game.res[i]);
}

// layers --------------------------------------------- //

Game.layers[1].extend = cc.Layer.extend({
    init: function () {
        this._super();
        var game = this;
        Game.layers[1].start(game);
    }
});

Game.layers[1].start = function (game) {
    game.addChild(new cc.LayerColor(new cc.Color(153, 92, 87)));

    let controller = new Controller(game);
    controller.start();
};

// scenes --------------------------------------------- //

Game.scenes[1].extend = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer = new Game.layers[1].extend();

        layer.init();
        this.addChild(layer);
    }
});

// start --------------------------------------------- //

cc.game.onStart = function () {

    cc.LoaderScene.preload(Game.g_resources, function () {
        cc.director.runScene(new Game.scenes[1].extend());
    }, this);
};

window.onload = function () {
    cc.game.run("gameCanvas");
};

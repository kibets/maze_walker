const NEW_LINE_MATCHER = /\r\n|\n\r|\n|\r/g;

const CELL_WALL = 0;
const CELL_EMPTY = 1;

const ASCII_EMPTY = " ";

/**
 * Minimal possible maze (1x1):
 * +
 *
 * Minimal possible maze (3x1):
 * +--+
 *
 * Minimal possible maze (1x3):
 * +
 * |
 * +
 *
 * Minimal canonical maze (3x3):
 * +--+
 * |  |
 * +--+
 */
class MazeParser {

    /**
     * All rows expected to have the same length.
     * Do not forget to include leading/trailing space characters (some editors could trim them on file save)
     *
     * @param asciiMaze {String} Jon's maze format (+--+)
     * @return {null|*|Array} parsed 2d grid, null in case of any format errors
     */
    static parse(asciiMaze) {
        if (typeof asciiMaze !== 'string') return null;

        let lines = this._split(asciiMaze);

        lines = lines.filter(line => line !== ""); // exclude empty lines, qol feature

        let rows = lines.length;

        if (rows === 0) return null;

        let expectedLineLength = lines[0].length;
        let cols = this._mazeWidth(expectedLineLength);

        if (!Number.isInteger(cols)) return null;

        let grid = ArrayUtils.make2dArray(cols, rows, CELL_EMPTY);

        for (let gy = 0; gy < rows; gy++) {
            let line = lines[gy];

            if (line.length !== expectedLineLength) return null;

            let gx = 0;

            for (let char = 0; char < line.length; char++) {
                if ((char + 2) % 3 === 0) continue;

                if (line[char] !== ASCII_EMPTY) {
                    grid[gx][gy] = CELL_WALL;
                }
                gx++;
            }
        }
        return grid;
    }

    static _mazeWidth(lineWidth) {
        return (lineWidth - 1) / 3 * 2 + 1;
    }

    static _split(lines) {
        return lines.replace(NEW_LINE_MATCHER, "\n").split("\n");
    }
}

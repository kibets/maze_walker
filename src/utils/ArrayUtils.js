class ArrayUtils {
    static make2dArray(cols, rows, value = undefined) {
        let array = [];
        for (let col = 0; col < cols; col++) {
            let r = [];
            array.push(r);
            for (let row = 0; row < rows; row++) {
                r.push(value);
            }
        }
        return array;
    }
}

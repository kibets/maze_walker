class Controller {
    constructor(game) {
        this._maze = null;

        this._game = new GameLayer(this);
        game.addChild(this._game, 0);

        this._gameUi = new GameUi();
        game.addChild(this._gameUi, 2);
    }

    start() {
        let area = document.getElementById("mazeInput");

        cc.loader.loadTxt(Game.res.maze_txt, (error, data) => {
            if (error) {
                console.error(error);
            } else {
                area.value = data;

                this.initNewMaze(data);
                this.solveMaze();
            }
        });

        area.addEventListener("change", () => {
            this.initNewMaze(area.value);
            this.solveMaze();
        });
    }

    initNewMaze(data) {
        let parsed = MazeParser.parse(data);

        if (parsed === null) {
            this._gameUi.showMessage("WRONG MAZE FORMAT"); // todo: i18n
            this._maze = null;

        } else {
            this._gameUi.showMessage(null);
            this._maze = new Maze(parsed);
        }

        this._game.setMaze(this._maze);
    }

    solveMaze() {
        if (this._maze === null) return;

        let start = this._maze.startPoint;
        let finish = this._maze.finishPoint;

        let path = this._maze.findPath(start.gx, start.gy, finish.gx, finish.gy);

        if (path.length === 0) {
            this._gameUi.showMessage("PATH NOT FOUND TO: [" + finish.gx + ", " + finish.gy + "]"); // todo: i18n
        }
        this._game.animateSolution(path, start, finish);
    }

    worldPosition(gx, gy) {
        if (this._maze === null) return cc.p(0, 0);

        return this._maze.worldPosition(gx, gy);
    }

    onTileVisit(gx, gy) {
        this._game.onTileVisit(gx, gy);
    }
}
